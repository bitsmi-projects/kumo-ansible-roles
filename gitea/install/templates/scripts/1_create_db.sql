--
-- CREATE USERS
--
DO $$
BEGIN
  CREATE USER gitea WITH PASSWORD '{{postgres_user_git_password}}';
  
  EXCEPTION WHEN DUPLICATE_OBJECT THEN
  RAISE NOTICE 'User gitea already exists';
END
$$;
	
GRANT ALL PRIVILEGES ON DATABASE gitea_db to gitea;
