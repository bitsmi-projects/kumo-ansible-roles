--
-- CREATE USERS
--
DO $$
BEGIN
  CREATE USER nextcloud WITH PASSWORD '{{postgres_user_nextcloud_password}}';
  
  EXCEPTION WHEN DUPLICATE_OBJECT THEN
  RAISE NOTICE 'User nextcloud already exists';
END
$$;
	
GRANT ALL PRIVILEGES ON DATABASE nextcloud_db to nextcloud;
