
Port 53 may be occupied in Ubuntu by the **systemd-resolved** service. in order to detect, execute the following command:

```sh
sudo lsof -i :53
```

If the command output states that the port is used by **systemd-resolved** service, this can be disabled on it's configuration:

* Edit `/etc/systemd/resolved.conf`
* Uncomment `DNS` and `DNSStubListener` lines and specify the following values

```
DNS=127.0.0.1
DNSStubListener=no
```

* Create a symbolic link for `/run/systemd/resolve/resolv.conf` with `/etc/resolv.conf` as the destination

```sh
sudo ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
```

* Reboot the system
